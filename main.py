import api_calls
import datetime
import json
import random

USAR_CACHE = True


def get_character(id: int) -> dict:
    query_start = datetime.datetime.now()

    if id in api_calls.r1 and USAR_CACHE:
        flag = True
        result = json.loads(api_calls.r1.get(id))

    elif id not in api_calls.r1 or not USAR_CACHE:
        flag = False
        response = api_calls.get_character(id)

        if response['code'] == 404:
            print("No existe el personaje con ID " + str(id))
            return None

        result = response['data']['results']

        s_data = None

        for obj in result:
            if str(obj['id']) == str(id):
                result = obj
                s_data = json.dumps(obj)
                break

        if s_data == None:
            print('Hubo un error.')
            return None

        if USAR_CACHE:
            api_calls.r1.set(id, s_data)

    query_end = datetime.datetime.now()

    if flag:
        print(f"{result['name']} - \033[92m En cache - {query_end - query_start} \033[0m")
    else:
        print(f"{result['name']} - \033[93m No en cache - {query_end - query_start} \033[0m" + ("\033[96m - Salio del cache \033[0m" if id in seen else ""))

seen = set()

def random_calls():
    CANTIDAD_DE_LLAMADAS = 100

    # Obtener ids aleatorias

    print("Creando lista de ids...")

    result = api_calls.get_characters()['data']['results']

    queries = []

    for x in range(CANTIDAD_DE_LLAMADAS):
        queries.append(result[random.randint(0, len(result) - 1)]['id'])

    # Llamar a la API por las ids

    print("Consultando a la API")
    before_ts = datetime.datetime.now()

    result = None
    flag = False
    for id in queries:
        get_character(id)
        seen.add(id)

    after_ts = datetime.datetime.now()

    print(f"Tiempo transcurrido: {after_ts - before_ts}")


while True:
    print(""" ---------- OPCIONES ----------
    1- Consultar personaje
    2- Llamadas aleatorias
    3- Limpiar cache
    """)

    r = int(input(" > "))

    if r == 1:
        print("Ingrese el id del personaje")
        c_id = int(input(" > "))

        get_character(c_id)
    
    elif r == 2:
        random_calls()

    elif r == 3:
        api_calls.clear_cache()

        print("Se limpio el cache")

    else:
        print("elija una opcion")
