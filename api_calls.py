import requests
import datetime
import hashlib
import redis
import random

r1 = redis.Redis(host='172.18.0.2', port=6379, decode_responses=True)
r2 = redis.Redis(host='172.18.0.3', port=6379, decode_responses=True)
r3 = redis.Redis(host='172.18.0.4', port=6379, decode_responses=True)

timestamp = datetime.datetime.now().strftime('%Y-%m-%d%H:%M:%S')
pub_key = 'ed061dc811c696730d629dcff75025a7'
priv_key = '868f0dd44ed8d5a8340bb31db5115af86c8ccd96'

def hash_params() -> str:
    hash_md5 = hashlib.md5()
    hash_md5.update(f'{timestamp}{priv_key}{pub_key}'.encode('utf-8'))
    hashed_params = hash_md5.hexdigest()

    return hashed_params


def clear_cache():
    r1.flushdb()
    r2.flushdb()
    r3.flushdb()


def get_characters() -> dict:
    offset = random.randint(0, 3)

    params = {'ts': timestamp, 'apikey': pub_key, 'hash': hash_params(), 'limit': 100, 'offset': offset * 100}

    response = requests.get(f'https://gateway.marvel.com:443/v1/public/characters', params=params)
    return response.json()


def get_character(id: int) -> dict:
    params = {'ts': timestamp, 'apikey': pub_key, 'hash': hash_params()}

    respuesta = requests.get(f'https://gateway.marvel.com:443/v1/public/characters/{id}', params=params)
    return respuesta.json()
